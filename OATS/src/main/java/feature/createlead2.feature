Feature: Create a Lead
Background: 
Given launch the browser
And Maximize the window 
And Set Timeouts
And enter URL

Scenario: Create a lead with CName FName and LName
And Enter UserName as DemoSalesManager
And Enter Password as crmsfa
And Click on Login Button
And Click on CRMSFA Link
And Click on Create Lead Link
And Enter Company Name as Capgemini
And Enter First Name as Syed Abdul Khader
And Enter Last Name as S
When Create Lead Button is Clicked
Then Lead Should be Created Successfully