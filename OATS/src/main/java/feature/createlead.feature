Feature: Create a Lead
Scenario Outline: Create a lead with CName FName and LName
And Enter UserName as <uname>
And Enter Password as <pwd>
And Click on Login Button
And Click on CRMSFA Link
And Click on Create Lead Link
And Enter Company Name as <cname>
And Enter First Name as <fname>
And Enter Last Name as <lname>
When Create Lead Button is Clicked
Then Lead Should be Created Successfully

#Scenario Outline: Create a lead with CName FName and LName
Examples:
|uname|pwd|cname|fname|lname|
|DemoSalesManager|crmsfa|cts|Syed|Khader|
|DemoSalesManager|crmsfa|CG|ABDUL|Khader|
