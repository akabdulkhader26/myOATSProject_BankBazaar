package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(id="updateLeadForm_companyName1")
	WebElement elecmpname;

	public EditLeadPage updatecmpname	(String data) {
		//WebElement elecmpname = locateElement("id", "updateLeadForm_companyName");
		type(elecmpname, data);
		return this;
	}
	public ViewLeadPage clickUpdate() {
		WebElement eleupdatebutton= locateElement("class", "smallSubmit");
		click(eleupdatebutton);
		return new ViewLeadPage(); 
	}
}
