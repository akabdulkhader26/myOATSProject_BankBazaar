package week2.collections;

import java.util.Scanner;

public class SunOfOd {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int number = scan.nextInt();
		SunOfOd odd = new SunOfOd();
		System.out.println(odd.sumOfODNum(number));
		scan.close();
	}
	
	public int sumOfODNum(int ip) {
		int sum = 0;
		while (ip>0) {
			int mod = ip%10;
			if (mod%2 != 0) {
				sum = sum+mod;  
			}
			ip = ip/10;
		}
		return sum;
	}
	
	
}






