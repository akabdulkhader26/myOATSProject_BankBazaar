package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnAlert {
	
	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/loginHome.jsf");
		
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		Thread.sleep(2000);
			
		driver.findElementById("loginbutton").click();
		Thread.sleep(5000);
		
		//Simple Alert - *can't take snap for Alert
		String text = driver.switchTo().alert().getText();		
		System.out.println(text);		
		
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img.png");
		FileUtils.copyFile(src, desc);
		
		
		driver.switchTo().alert().accept();
		TargetLocator switchTo = driver.switchTo();
		
		
		
		driver.findElementById("usernameId").sendKeys("nandhini");
		
		
	}
	
	
	
	
	
	
	
	
	

}
