package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnFrame {
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://jqueryui.com/selectable/");
		
		//Enter into the Frame
		WebElement frame = driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frame);
		
		driver.findElementByXPath("//li[text()='Item 2']").click();
		//Come Out from frame
		driver.switchTo().defaultContent();
		
		driver.findElementByLinkText("Download").click();
		
	}
	
	
	
	
	
	
	
	

}
