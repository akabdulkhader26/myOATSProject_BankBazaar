package week3.day2;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TakeSnap {

	public static void main(String[] args) throws 
	IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		// TimeOut - Selenium wait
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		
		driver.findElementByXPath("//input[@id='username']")
		.sendKeys("DemoSalesManager");

		driver.findElementById("password")
		.sendKeys("crmsfa");
		// WebDriverWait
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable
				(By.className("button")));
		driver.findElementByClassName("button").click();







































		// ScreenShot
		// store output as file
		File source = driver.getScreenshotAs(OutputType.FILE);
		// store the file in the specific path
		File dest = new File("./Snaps/img.png");
		// Copy src to dest
		FileUtils.copyFile(source, dest);



















	}

}











